#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <fstream>
#include <chrono>

#include <limits>
const auto INF = std::numeric_limits< uint64_t >::max() ;
template<typename TimeType>
struct TaskStruct
{
	TaskStruct() = default;
	/*TaskStruct(uint32_t dur)
		: TaskStruct(TimeType(dur)) {}*/
	TaskStruct(TimeType duration)
		: duration(duration) 
		, ES(0u)
		, EF(duration)
		, LS(0u)
		, LF(duration) {}
	TaskStruct(TimeType duration, TimeType ES, TimeType EF, TimeType LS, TimeType LF)
		: duration(duration)
		, ES(ES)
		, EF(EF)
		, LS(0u)
		, LF(duration) {}

	TaskStruct(const TaskStruct& other) = default;
	TaskStruct(TaskStruct&& other) = default;
	TaskStruct& operator=(const TaskStruct& other) = default;

	TimeType duration, ES, EF, LS, LF;
	bool isSane() const {
		return (ES + duration == EF) && (LS + duration == LF) && (EF <= LF);
	}
	
	typedef TimeType value_type;
};

typedef TaskStruct<uint64_t> Task;
typedef boost::adjacency_list<boost::listS, boost::vecS, boost::bidirectionalS, Task> AdjacencyList;

template<typename Vertex>
void relax(Vertex& u, Vertex& v)
{
	if (v.ES < u.EF) {
		v.ES = u.EF;
		v.EF = v.ES + v.duration;
	}

	if (v.LF < v.EF) {
		v.LF = v.EF;
		v.LS = v.ES;
	}

	if ((u.EF < v.LS && u.LF <= u.EF) || v.LS < u.LF) {
		u.LF = v.LS;
		u.LS = u.LF - u.duration;
	}

	assert(u.isSane() && v.isSane());
}

template<typename GraphType = AdjacencyList>
void topologicallySortedCPM(GraphType& graph)
{
	typedef typename boost::graph_traits<GraphType>::vertex_descriptor Vertex;
	typedef typename std::remove_reference<decltype(graph[0])>::type TaskType;
	typedef typename TaskType::value_type TimeType;

	std::vector<Vertex> sortedVertices;
	TimeType maxEF {0u};
	sortedVertices.reserve(boost::num_vertices(graph));

	boost::topological_sort(graph, std::back_inserter(sortedVertices)); //O(V+E)

	for (auto& vertex : boost::adaptors::reverse(sortedVertices)) {
		auto edgeRange = boost::in_edges(vertex, graph);

		for (auto edge = edgeRange.first; edge != edgeRange.second; ++edge) {
			auto& u = graph[boost::source(*edge, graph)];
			auto& v = graph[boost::target(*edge, graph)];

			if (v.ES < u.EF) {
				v.ES = u.EF;
				v.EF = v.ES + v.duration;
			}
			v.LF = v.LS = INF;
			u.LF = u.LS = INF;
			maxEF = std::max(v.EF, maxEF);
		}
	}
	auto projectTime = TimeType {0u};
	for (auto& vertex : sortedVertices) {
		auto edgeRange = boost::out_edges(vertex, graph);
		auto& task = graph[vertex];

		if (edgeRange.first == edgeRange.second) {
			task.LF = maxEF;
			task.LS = maxEF - task.duration;
		}
		
		auto minLF = task.LF;
		for (auto edge = edgeRange.first; edge != edgeRange.second; ++edge) {
			auto& u = graph[boost::source(*edge, graph)];
			auto& v = graph[boost::target(*edge, graph)];
			
			if (v.LS < u.LF) {
				minLF = std::min(minLF, v.LS);
			}
		}
		
		task.LF = minLF;
		task.LS = task.LF - task.duration;
		
		projectTime = std::min(minLF, projectTime);
	}
	std::cout << projectTime << std::endl;
}

template<typename GraphType = AdjacencyList>
void bellmanFordCPM(GraphType& graph) 
{
	typedef typename std::remove_reference<decltype(graph[0])>::type TaskType;
	typedef typename TaskType::value_type TimeType;

	auto edgeRange = boost::edges(graph);
	auto V = boost::num_vertices(graph);
	TimeType maxTime {0ull};

	for (auto i = 0ull; i < V-1; ++i) {
		for (auto& edge = edgeRange.first; edge != edgeRange.second; ++edge) {
			auto target = boost::target(*edge, graph);
			auto source = boost::source(*edge, graph);
			auto& u = graph[source];
			auto& v = graph[target];
			auto outEdges = boost::out_edges(target, graph);

			if (outEdges.first == outEdges.second) {
				v.LF = maxTime;
				v.LS = maxTime - v.duration;
			}

			relax(u, v);
			maxTime = std::max(v.EF, maxTime);	
		}
	}

	 //set correct LF & LS for unconnected vertices
	for (auto i = 0u; i < V; ++i) {
		auto outEdges = boost::out_edges(i, graph);
		auto& v = graph[i];
		if (outEdges.first == outEdges.second) {
			maxTime = std::max(maxTime, v.duration);
			v.LF = maxTime;
			v.LS = maxTime - v.duration;
		}
	}
}

template<typename GraphType = AdjacencyList>
GraphType loadGraph(const std::string& fileName)
{
	GraphType graph;
	typedef typename std::remove_reference<decltype(graph[0])>::type TaskType;

	std::ifstream input {fileName};
	if (!input.is_open()) {
		throw;
	}

	auto V = 0ull, E = 0ull;
	auto duration = 0ull;
	input >> V; input >> E;
	for (auto i = 0ull; i < V; ++i) {
		input >> duration;
		if (!input.good()) {
			throw;
		}

		TaskType task {duration};
		auto v = boost::add_vertex(graph);
		graph[v] = task;
	}

	auto v = 0ull, u = 0ull;
	for (auto i = 0ull; i < E; ++i) {
		input >> u >> v;
		if (!input.good()) {
			throw;
		}

		boost::add_edge(u-1, v-1, graph);
	}

	return std::move(graph);
}

template<typename GraphType, typename VertexType>
void printCriticalPath(const GraphType& graph, VertexType vertex) {
	auto edgeRange = boost::out_edges(vertex, graph);
	for (auto edge = edgeRange.first; edge != edgeRange.second; ++edge) {
		auto target = boost::target(*edge, graph);
		auto& task = graph[target];

		if (task.EF != task.LF) {
			continue;
		}

		std::cout << task.ES << " " << task.EF << " " << task.LS << " " << task.LF << std::endl;
		printCriticalPath(graph, target);
	}
}

template<typename GraphType>
void printCriticalPaths(const GraphType& graph) {
	typedef typename boost::graph_traits<GraphType>::vertex_descriptor Vertex;

	std::vector<Vertex> sortedVertices;
	boost::topological_sort(graph, std::back_inserter(sortedVertices));
	
	for (const auto& vertex : sortedVertices) {
		auto inEdges = boost::in_edges(vertex, graph);
		auto& task = graph[vertex];

		if (task.EF != task.LF || inEdges.first != inEdges.second) {
			continue;
		}

		std::cout << "Critical path: " << std::endl;
		std::cout << task.ES << " " << task.EF << " " << task.LS << " " << task.LF << std::endl;

		printCriticalPath(graph, vertex);
		std::cout << std::endl;
	}
}

int main(int argc, char **argv) 
{
	auto graph = loadGraph("plik.txt");
	bellmanFordCPM(graph);
	//topologicallySortedCPM(graph);
	
	auto vertexRange = boost::vertices(graph);
	
	for (auto vertex = vertexRange.first; vertex != vertexRange.second; ++vertex) {
		auto& task = graph[*vertex];
		std::cout << task.ES << " " << task.EF << " " << task.LS << " " << task.LF << std::endl;
	}
	
	std::cout << std::endl;
	
	printCriticalPaths(graph);
	return 0;
}


